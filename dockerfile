# Use the Python 3.9 image as the base image
FROM python:3.9

# Set the working directory in the container
WORKDIR /app

# Copy the contents of the current directory to the working directory
COPY . .

# Install the required packages from the requirements.txt file
RUN pip install --no-cache-dir -r requirements.txt

# Expose port 9000 for incoming requests
EXPOSE 9000

# Set the entrypoint command to start the Python service
ENTRYPOINT ["python"]

# Set the command to run when the container starts
CMD ["service/app.py"]
